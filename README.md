# Sabre - un référenciel d'accompagnement à l'ouverture de…

# données
# code source
# documentation

## Status

![](https://gitlab.com/jibe-b/sabre/raw/results/results-badge.png)

## Description

L'ouverture de données, code source, documentation peut être rendue difficile par de nombreux facteurs, comme :
- la complexité de la tâche, souvent collosale
- la honte de montrer un contenu pas complètement parfait

## Proposition

Afin de guider l'auteur·e d'un contenu à l'ouvrir, ce bot scanne des répos git et propose un rapport sur l'avancement par rapport à des objectifs d'ouverture.

La validation ou l'absence de validation d'un critère est accompagnée par des guides pour avancer vers la validation de ce critère.

Lorsque des outils sont disponibles pour valider un critère de manière automatisée ou semi-automatisée, des tutoriels pour ces outils sont proposés.

## Raison d'être basé sur git

Git permet de versionner un contenu. Par conséquent, il est possible de garder trace des étapes ayant permis de valider chacun des critères.

Cela permet de faire des tentatives qui si elles se concluent par un échec donneront lieu à un retour à l'état initial sans difficulté.

Le process d'accompagnement aura toujours un caractère inabouti, même s'il s'en approche. Le fait de versionner les échecs permettra d'identifier les chemins qui s'avèrent bloquants et ainsi d'améliorer le process.

## Référencement des répos (centralisé)

Ce bot peut être exécuté de manière centralisé sur une liste de répos. Cela donne un tableau de validation (rouge/vert) de critères.

La liste des répos suivis par le bot est dans le fichier [liste-de-repos-a-scanner.csv](liste-de-repos-a-scanner.csv)

## Exécution du bot dans un répo (décentralisé)

Le bot peut également être exécuté dans un répo isolé, évitant la centralisation. Un script est mis à disposition pour télécharger le code à exécuter sur un runner auto-hébergé.

Pour cela, copiez-collez le fichier dédié [.gitlab-ci.yml](decentralized/.gitlab-ci.yml) et placez-le à la base de votre répo (attention, ne copiez pas le .gitlab-ci.yml à la base de ce répo).

## Jeux de critères

- critères de Mozilla Open Leaders
- Modèle 5 étoiles du Linked Open Data https://5stardata.info/en/

## Accéder aux notebooks de développement

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fsabre/master?urlpath=lab%2Ftree%2Fevaluate_data_repo-tuto.ipynb)
