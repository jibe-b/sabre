import evaluate_data_repo

def test_evaluate_repo_returns_un_entier():
    expected = type(int())
    actual = type(evaluate_data_repo.evaluate_repo())
    
    assert(expected == actual)