# Liste de features, associées aux tests

## Liste de répos à test

La liste contenue dans [liste-de-repos-a-scanner.csv](liste-de-repos-a-scanner.csv) correspond aux répos qui seront scannés.

## Liste de critères par répo

Par défaut, l'ensemble des critères est testé pour chaque répo.

Un opt-in (ou opt-out) pour les listes de critères par répo sera ajouté.

## Scan centralisé

- test : tâche "CI" : scan_repos 


## Scan décentralisée

- test : pipeline exécuté dans un répo


## Affichage des résultats de pipeline

- Un bot (hébergé sur un VPS) récupère le dernier tableau de résultats et le push sur le répo.

- le fichier resultats.csv affiche les résultats pour l'ensemble des répos

## Fourniture d'un fichier de résultats par répo

Un dossier par répo sera créé automatiquement, avec un fichier contenant les résultats. L'URL de ce fichier sera stable et pourra dont être utilisée comme badge (mis à jour) par les auteurs du répo.


## Liste de critères

Des critères sont fournis par différentes entités.

Par défaut, l'ensemble des critères sont exécutés.

Les résultats sont placés dans le fichiers resultats.csv

### Évaluation des critères "OpenLeaders" de Mozilla

- Branche openleaders-criteria : évalue les critères OpenLeaders

### Évaluation des critères "OpenessEtalab" d'Etalab

### Évaluation des critères "Sécurité"

### Évaluation des critères "Modularité"

### Évaluation des critères "?"
